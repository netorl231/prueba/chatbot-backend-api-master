package pe.confianza.chatbot.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.confianza.chatbot.api.config.MessengerCfg;

@RestController
@RequestMapping("/callback")
public class ChatbotController {
	
	private Logger logger = LoggerFactory.getLogger(ChatbotController.class);
	
	@Autowired
	private MessengerCfg _messengerCfg;
	
	@GetMapping
	public ResponseEntity<?> verifyWebHook(@RequestParam("mode") String mode,
										@RequestParam("verify_token") String verify_token,
										@RequestParam("challenge") String challenge){
				
		Map<String, Object> response = new HashMap<>();
		
		logger.info("Recibiendo webhook");
		logger.info("AppSecret: " + _messengerCfg.getAppSecret());
		logger.info("PageAccessToken: " + _messengerCfg.getPageAccessToken());
		logger.info("VerifyToken: " + _messengerCfg.getVerifyToken());
	
		try {
			if(!mode.equals("subscribe")) {
				response.put("error", "falló la verificación de webhook. El mode : '" + mode +"', es inválido.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			if(!verify_token.equals(_messengerCfg.getVerifyToken())) {
				response.put("error", "falló la verificación de webhook. El token de verificación : '" + verify_token +"', es inválido.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}catch(Exception e) {
			logger.error("Error validando webhook: " + e.getMessage());
			response.put("error", "falló la verificación de webhook." + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(challenge, HttpStatus.OK);
	}

}
