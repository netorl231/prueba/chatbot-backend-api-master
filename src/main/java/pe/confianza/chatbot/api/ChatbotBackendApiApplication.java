package pe.confianza.chatbot.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatbotBackendApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatbotBackendApiApplication.class, args);
	}

}
